Video-on-Demand
=================================

Die Anforderungen wurden bereits im einführenden Kapitel zum Thema Video erörtert. Videoformate müssen entsprechend der Endgeräte bzw. Browser gewählt werden. Zudem sollten die Videos in entsprechender Qualität und Auflösung bereitgestellt werden. Angenommen es sind 500 Nutzer zu Beginn online und möchten regelmäßig das breite \ac{VoD} Angebot nutzen. Jedes Video muss in den drei gängigen Formaten (webm, ogg, mpeg-4) und Auflösungen zum Senden bereit stehen. Im Livestreaming-Fall mussten lediglich Daten von drei Streams gecached werden. Da jetzt pro Video mindestens 9 Dateien vorliegen müssen und bei Bedarf auch jedes der Videos angefordert werden kann und simultan verarbeitet werden muss, erhöhen sich der Speicherbedarf und die notwendige Rechenleistung. Um diesen Herausforderungen gerecht zu werden, wird die Nutzung eines bestehenden \ac{CDN} in Betracht gezogen. Amazon bietet mit seinen \ac{AWS} ein interessantes Geschäftsmodell. Von Bedeutung für das Online-Rückenstudio sind die beiden Dienste \ac{S3} und \ac{CF}. \ac{S3} dient als Online-Speicher aller Videos und CF dient als \ac{CDN}. Durch die Nutzung der \ac{AWS} erübrigt sich das Problem mit der Skalierbarkeit der Anwendung. 

Eine weitere Anforderung an die VoD-Komponente ist, dass MitarbeiterInnen von ViaVitale neue Trainingsvideos schnell und einfach ohne Wissen über die verwendeten Techniken zu dem bestehen Angebot hinzufügen können. Zu diesem Zweck wird ein *Admin-Interface* erstellt. Neben diversen Kategorien soll das entsprechende Video von einem lokalen Speicherplatz gewählt werden können. Durch die Verwendung von *Rails-Erweiterungen* werden die Videos vor dem Abspeichern in einer S3-Instanz lokal in die drei gängigen Formate (mp4, webm, ogg/theora) in unterschiedlichen Auflösungen und Bitraten mit Hilfe von *FFmpeg* konvertiert.

Im folgenden werden die Amazon Web Services S3 und CF erläutert und die Implementierung in die Anwendung anhand von ausgewählten Beispielen verdeutlicht. Es folgt eine Einführung in *FFmpeg* und *Paperclip*. In einem zweiten Teil wird die Funktionsweise des *Admin-Interfaces* beschrieben und auf das Zusammenspiel von *FFmpeg*, *Paperclip* und *AWS* eingegagen.


Amazon Web Services 
-----------------------------------

Die \ac{AWS} beinhalten eine Vielzahl von Cloud-Lösungen. Neben kompletten Recheninstanzen (EC2) können auch Datenbanken (SDB) bzw. die schon genannten Dienste S3 und CF genutzt werden. Insgesamt stehen 18 Dienste für Entwickler und Privatanwender bereit. Die Kosten für die Nutzung der Dienste skaliert mit der Größe der Anwendung. Es wird nur das gezahlt, was auch genutzt wird. Ist die Rechenleistung einer EC2-Instanz nicht mehr ausreichend, kann einfach auf die nächst schnellere Variante aufgestuft werden. Ein Abbild der alten Konfiguration wird auf die neue Instanz gespielt und macht das neue System sofort lauffähig. Alle Dienste bieten zudem eine Entwickler API. Für \ac{RoR} stehen Erweiterungen wie das *aws-sdk*-Gem bereit.  Mit Hilfe des *aws-sdk*-Gems können Verbindungen zum eigenen Cloud-Dienst aufgebaut werden. Das ist wichtig, wenn hochgeladene Dateien direkt in einem S3-Bucket einzufügen sind.

### Amazon S3

Amazon S3 ist ein Cloud-Service zum Speichern von Daten in einem sogenannten *Bucket*. Ein *Bucket* befindet sich in einem von 8 Serverzentren (3x USA, 1x EU(Irland), 3x Asien, 1x Südamerika).

Über die Webschnittstelle können eine unbegrenzte Anzahl von Dateien zwischen 1 Byte und 1 TByte gelesen, geschrieben und gelöscht werden. Dateien, die in einem Bucket gespeichert wurden, können nur mit einem vom Entwickler festgelegten Schlüssel abgerufen werden. Objekte stehen je nach Wunsch öffentlich oder privat bereit. Amazon sichert zudem eine 99,999999999 %-ige Zuverlässigkeit und 99,99 %-ige Verfügbarkeit von Objekten über einen Zeitraum eines Jahres zu \cite{awss3:2006}.

Die Kosten für den Dienst setzen sich zusammen aus dem benötigten Datenspeicher, der Anzahl der Anfragen und dem Datenvolumen. Die Tabelle 3.7 zeigt einen Auszug aus der Preisliste.

: Anfragegebühren S3-EU (Irland) \cite{awscf:2013}

+------------------------------------------------------------+------------------------------+
| Vorgang                                                    | Kosten                       |
+============================================================+==============================+
| PUT-, COPY-, POST- oder LIST-Anforderungen                 | $0,005 pro 1.000 Abfragen    |
+------------------------------------------------------------+------------------------------+
| Glacier-Archivierungs- und Wiederherstellungsanforderungen | $0,055 pro 1.000 Abfragen    |
+------------------------------------------------------------+------------------------------+
| Löschanforderungen                                         | Free                         |
+------------------------------------------------------------+------------------------------+
| GET- und alle anderen Anforderungen                        | $0,004 pro 10.000 Anfragen   |
+------------------------------------------------------------+------------------------------+
| Glacier-Datenwiederherstellungen                           | Free                         |
+------------------------------------------------------------+------------------------------+

Da Videoinhalte der zentrale Bestandteil des Online-Rückenstudios sind, spricht die hohe Zusicherung an Verfügbarkeit und Zuverlässigkeit für den S3-Dienst. Zudem verfügt S3 über eine Schnittstelle, über die mit Amazon's \ac{CF} kommuniziert werden kann. 

### AmazonCloudFront

CF ist darauf ausgelegt mit anderen Amazon Diensten zu kommunizieren und z.B. im S3 abgelegte Objekte bereitzustellen. Die vorgestellten Informationen und Konfigurationen orientieren sich an dem offiziellen CF-Developer-Guide \cite{awscf:2013}. Das CF Netzwerk verfügt über global verteilte *Edge-Standorte*. Wird ein S3-Bucket zum CF-Dienst hinzugefügt, so werden alle dort abgelegten Daten an den *Edge-Standorten* gecached, um eine bestmögliche Leistung bei einer Anfrage einer oder mehrerer Dateien zu gewährleisten. Inhalte können statisch, dynamisch oder als Stream bereitgestellt werden. Daten können entweder per HTTP, HTTPS (statischer Download) oder RTMP (streaming) bereitgestellt werden. Jedes CF-Verteilersystem verfügt über einen eigenen \ac{DN}. Dieser kann nach erfolgreicher Konfiguration in der Webanwendung eingebunden werden.

Beim Erstellen einer CF-Instanz stehen Entwicklern diverse Optionen zur Verfügung. Es ist wichtig, dass die Ursprungsdatei dahingehend geschützt ist, dass nur angemeldete Nutzer z.B. mit Abo-Status Trainingsvideos abrufen können. 

Zunächst muss der direkte Zugriff auf ein *S3-Bucket* beschränkt werden (Abb. 3.8). Von jetzt an können Objekte im *S3-Bucket* nur noch über CF-URLs aufgerufen werden.

\begin{figure}[H]
\centering
\includegraphics[width=0.5\textwidth]{../figures/CF_PrivateContent.png}
\caption{CloudFront Zugriffs Beschränkung auf ein S3-Bucket \cite{awscf:2013} }
\end{figure}

Eine CF-URL ist zwar eindeutig, aber steht allen zur Verfügung, die die URL kennen. Um den Zugriff weiter zu beschränken, stehen sogenannte *Signed-URL's* zur Verfügung. Eine *Signed-URL* besteht aus 5 Teilen: Der Basis-URL, einem Ablaufdatum und einer Ablaufzeit, einem *Policy Statement*, einer Signatur und einer CF-Schlüsselpaar-ID. Ruft ein Nutzer mit einer *Signed-URL* eine Datei ab, überprüft CF ob die Anfrage gültig ist. Eine Anfrage ist dann gültig, wenn das rekonstruierte *Policy Statement* mit dem *Policy Statement* in der Signatur übereinstimmt und die Anfrage noch nicht abgelaufen ist (Ablaufdatum und -zeit > aktueller Zeitpunkt). 

In der Webanwendung müssen gültige *Signed-URLs* pro Anfrage und Nutzer mit entsprechender Berechtigung erstellt werden. Dabei kann zwischen einer Standard (*canned*) oder benutzerdefinierten (*custom*) Police gewählt werden. Eine *canned-Policy* eignet sich, um den Zugriff auf einzelne Objekte zu beschränken und die Zugriffskontrolle über einen Ablaufzeitpunkt geregelt werden soll. Eine *custom-Policy* erweitert die Zugriffskontrolle für eine Menge von Objekten über einen ganzen Zeitraum und kann Zugriffsrechte für einen IP-Adressen-Raum geben. Beide Policen besitzen ein *statement*. Darin wird eine Ressource und beliebig viele Bedingungen angegeben. Im vorliegenden Beispiel für eine *custom-Policy* (Listing 3.1) werden alle Ressourcen, die im "````/videos/*```` Ordner" in der ViaVitale CF-Instanz liegen zwischen dem *26.08.2013 14:30:30* und dem *26.08.2013 15:30:30* Uhr für alle Anfragen aus dem Adressraum *192.0.2.10/32 freigegeben.

\begin{lstlisting}[caption={Codebeispiel für eine \textit{custom-Policy}}]
{ 
   "Statement": [{ 
      "Resource":"http://s2yx5dnojzkrry.cloudfront.net/viavitale/videos/*",
      "Condition":{ 
         "IpAddress":{"AWS:SourceIp":"192.0.2.10/32"},
         "DateGreaterThan":{"AWS:EpochTime":1377545430},
         "DateLessThan":{"AWS:EpochTime":1377549030}
      } 
   }] 
}
\end{lstlisting}

Da in der Anwendung einzelne Nutzer einzelne Videos abrufen möchten und die URL nach Ablauf einer Zeitspanne ungültig werden soll, reicht eine *canned-Policy* aus. Um eine *signed-URL* mit einer *canned-Policy* zu erstellen müssen im Controller der Webanwendung 6 Teile zu einer URL zusammengefügt werden (Figure 3.9).

\newpage

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{../figures/CF_cannedurl.png}
\caption{Cloud Front - Teile einer \textit{canned-signed-URL}}
\end{figure}

Zunächst wird die Basis-URL genommen und durch ein *"?"* erweitert. Es folgen optionale Parameter, wie *size* oder *quality*. Die folgenden drei Parameter sind die entscheidenden. *Expires* gibt an bis wann (Unix Zeitstempel^[Ein Unix Zeitstempel errechnet sich aus der Differenz vom 1. Januar 1970 und dem angegebenen Auslaufzeitpunkt]) die URL gültig ist. Die *Signature* ist eine *gehashte* und *signierte* Version des *Policy-Statements*. Appendix G zeigt eine mögliche Ruby-Implementation um sowohl *canned-* als auch *custom-Policies* zu generieren.

Ist die URL erstellt und gültig kann sie als *source* Parameter im HTML5 Video-Tag für einen berechtigten Nutzer verwendet werden (HAML: Listing 3.2 und JS: Listing 3.3).

\begin{lstlisting}[caption={HAML: Verwendung einer \textit{signed-URL}}]
.row
  %h3
  MP4 HD
  .span9
    .vod1
      %video
        %source{ :type => "video/mp4", 
          :src => "#{AWS::CF::Signer.sign_path(
            '@video.attachment.url(:mp4_hd,false) 
              + @video.attachment.path(:mp4_hd)', 
            :expires => Time.now + 600)}"}
\end{lstlisting}

\begin{lstlisting}[caption={Verwendung einer \textit{signed-URL} im Frontend}]
function watch_video(id){
  LoadContent("videos/watch/"+id, function(){
    $(".vod1").flowplayer({
      swf: "/flowplayer-5.4.2/flowplayer.swf",
      rtmp: "rtmp://s2yx5dnojzkrry.cloudfront.net/cfx/st"
    });
  });
}
\end{lstlisting}

Abgespeicherte Videos können mit der vorgestellten Konfiguration von Nutzern mit Berechtigung abgerufen werden. Um neue Videos zum *BackZoom-Angebot* hinzufügen zu können müssen die Videos sowohl im S3-Bucket abgelegt werden und die Datenbankeinträge müssen entsprechend erweitert werden. 

Admin-Interface
-----------------------------------

Über ein Admin-Interface, welches von *Rails* durch die *ActiveAdmin-Erweiterung* bereitgestellt wird, können MitarbeiterInnen von ViaVitale nach erfolgreicher Anmeldung zum *Dashboard* unter */admin/videos/new* (Abb. 3.10) neue Videos zu dem bestehenden Angebot hinzufügen. 
Das ausgewählte Video sollte in maximaler Qualität vorliegen, da es in diversen Schritten in die drei gängigen Formate (webm, ogg, mpeg-4) mit unterschiedlichen Auflösungen und Bitraten umgewandelt wird. Sollte die vorliegende Qualität zu gering sein, können *HD*-Versionen nicht in gewünschter Qualität erstellt werden. 

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{../figures/Admin_Interface_VoD.png}
\caption{Ausschnitt aus dem Admin-Interface \textit{/admin/videos/new}: MitarbeiterInnen Ansicht um ein neues Video hochzuladen.}
\end{figure}

Nachdem in der Eingabemaske alle wichtigen Informationen, Kategorien und Beschreibungen eingegeben wurden und *Create Video* ausgeführt wird, laufen im Hintergrund mehrere Schritte ab:

1. Angegebene Attribute werden gesetzt 
2. Der Anhang wird auf ein gültiges Video-Format überprüft
3. Das Video wird entsprechend der Werte aus Tabelle 3.4 von *FFmpeg* konvertiert
4. Die konvertierten Videos werden in entsprechenden Ordnern innerhalb des S3-Buckets abgelegt.
5. Wenn kein Fehler verursacht wurde wird der/die AnwenderIn mit einer Erfolgsmeldung informiert.
 
Paperclip und aws-sdk Implementierung
-----------------------------------

Die Schritte 2 bis 4 aus dem vorherigen Absatz werden von *Paperclip* und *aws-sdk* übernommen. Zunächst muss die S3-Kommunikation konfiguriert werden. In einer Rails-Konfigurationsdatei *./config/environments/production.rb* werden Standardwerte für *Paperclip* gesetzt (Listing 3.4). Als Speicherinstanz-Typ wird *s3* mit der Standard *s3_eu_url* angegeben. Passwörter und Schlüssel liegen in einer externen Datei unter *./config/s3_credentials.yml* und werden zusätzlich geladen. Damit die hochgeladenen Inhalte nicht öffentlich sind werden die Zugriffsrechte auf *private* gesetzt.   

\begin{lstlisting}[caption={Auszug aus dem Quellcode: production.rb}]
config.paperclip_defaults = {
  :storage => :s3,
  :url => ':s3_eu_url',
  :s3_credentials => YAML.load_file('config/s3_credentials.yml'),
  :s3_permissions => :private
}
\end{lstlisting}

Als nächstes müssen in der Video Klasse (Appendix H) Ergänzungen vorgenommen werden.  

    # Auszug aus dem Quellcode
    # ./app/models/video.rb
\lstinputlisting[firstnumber=15, firstline=15, lastline=33]{code/video.rb} 

Zeile 14 bis 29 bilden die Kernkonfiguration für alle hochzuladenen Videos. Die Funktion \linebreak `` has_attached_file `` nimmt die Objektreferenz des Videos ```` :attachment ```` und optional Werte für ```` :path ````, ```` :styles ```` und ```` :processors ````. ```` :path ```` gibt an wo eine Datei hochgeladen werden soll. Der angegebene Pfad ```` videos/:trainer/:style/:basename.:content_type_extension ```` wird durch Einträge in der Eingabemaske (s. Admin-Interface) angepasst. Wird ein Video mit dem Namen "Einführung in die Rückenschule" von Heike als ``:mp4_hd`` hochgeladen, liegt die Datei im S3-Bucket unter *./videos/Heike/mp4_hd/Einführung\_in\_die\_Rückenschule.mp4*. ```` :styles ```` beinhaltet Hashes, die später für den *Preprocessor* wichtig sind. In den Zeilen 17 bis 27 werden *Styles* definiert. Jeder *Style* bekommt einen Hash-Schlüssel (z.B. *mp4_hd* oder *ogv_sd*), dem eine weitere Hash mit Werten für die Geometrie und das Containerformat zugewiesen wird.

Wenn es sich bei der ausgewählten Datei tatsächlich um ein Video handelt, wird der *Preprocessor* mit den Werten aus der *:styles*-Hash aufgerufen. Dieser baut FFmpeg-Befehle auf und führt diese lokal aus. Nachdem alle Konvertierungen erfolgreich abgeschlossen wurden, wird Schritt 4 ausgeführt. Der *Preprocessor* wurde von Omar Abdel-Wahab \cite{Abdel-Wahab:2013:Online} entwickelt und frei zur Verfügung gestellt. Anpassungen sind notwendig, um die gewünschten Endformate erzeugen zu können und die vorhandene FFmpeg-Installation korrekt aufrufen zu können (vgl. Appendix I).

Bitraten
-----------------------------------

Die durch *FFmpeg* erstellten Videos haben nach der Konvertierung die Werte aus Tabelle 3.8 angenommen und befinden sich abrufbereit auf dem Server. Die *webm*-Konvertierung erfüllt noch nicht die Qualitätsanforderungen. Bei der *mp4*- und *ogv*-Konvertierung sind die Bitraten für die HD Qualität etwas zu hoch. Durch das Verändern der Parameter im *Paperclip*-Preprocessor können die Resultate noch weiter verbessert werden. Das ist die Aufgabe des Videotechnikers und kann im Rahmen dieser Arbeit nicht detailliert ausgeführt werden.

\newpage

:Auflösung und Bitraten nach der Konvertierung eines Videos

+---------+---------------+----------------+----------+
| Style   | Auflösung[px] | Bitrate [kbps] |  Format  |
+=========+===============+================+==========+
| ogv_hd  | 1920x1080     | ~3200          | ogv      |
+---------+---------------+----------------+----------+
| ogv_sd  | 720x480       | ~750           | ogv      |
+---------+---------------+----------------+----------+
| ogv_lo  | 480x320       | ~350           | ogv      |
+---------+---------------+----------------+----------+
| mp4_hd  | 1920x1080     | ~3500          | mpeg-4   |
+---------+---------------+----------------+----------+
| mp4_sd  | 720x480       | ~800           | mpeg-4   |
+---------+---------------+----------------+----------+
| mp4_lo  | 480x320       | ~400           | mpeg-4   |
+---------+---------------+----------------+----------+
| webm_hd | 1920x1080     | ~900           | webm     |
+---------+---------------+----------------+----------+
| webm_sd | 720x480       | ~230           | webm     |
+---------+---------------+----------------+----------+
| webm_lo | 480x320       | ~200           | webm     |
+---------+---------------+----------------+----------+

