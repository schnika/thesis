Livestreaming
==================================

Im Folgenden werden alle notwendigen Schritte zur Konfiguration von Hard- und Software mit Erläuterungen aufgeführt. 

Die Livestreaming-Komponente verlangt anders als die später vorgestellte \ac{VoD}-Komponente die direkte Interaktion zwischen dem ViaVitale-Team und der Hardware bestehend aus Kamera und Computer. Für die richtigen Kameraeinstellungen und richtige Verkabelung sind später Mitarbeiter vor Ort verantwortlich. Eine einfache Interaktion und Konfiguration des Livestreaming-Clients ist daher notwendig. 

Neben der Nutzerinteraktion spielt auch die Beachtung der unterschiedlichen Endgeräte und Breitbandverfügbarkeiten eine Rolle.
Um die unterschiedlichen Breitbandverfügbarkeiten abzudecken, ist es notwendig, den Video-Stream in unterschiedlichen Bitraten bereitzustellen. 
Während stationäre PCs und Laptops mit aktuellen Browsern keine Probleme mit dem \ac{RTMP} haben, muss für IOS- und Android-Geräte ein alternatives Protokoll zum Einsatz kommen, wenn man den Endnutzern keine zusätzliche Arbeit mit der Installation von Plugins o.ä. machen möchte.

Die Streaming-Architektur besteht aus einem Streaming-Client und einem oder mehreren Streaming-Servern. Mit dem Streaming-Client ist eine hochauflösende Kamera verbunden. Für die Übertragung des Video- und Audiosignals kommt der \ac{FMLE} zum Einsatz. Serverseitig läuft ein Open-Source C++ RTMP-Server (Kapitel 3.3.3). Dieser kann beliebig viele (abhängig von der Hardware) Streams via RTMP/RTSP zur Verfügung stellen. In der Webanwendung läuft ein Flashplayer (Flowplayer), welcher den RTMP-Stream abruft.

Anforderungen
----------------------------------

Ein Streaming-Client muss ausreichend Upload-Kapazitäten für den Multi-Bitrate-Stream haben. Laut [Livestreaming.org](www.livestreaming.org) sollten nur 40-50% der Upstream-Kapazitäten verwendet werden, um Schwankungen und Instabilitäten auszugleichen.

Diverse Sicherheitsaspekte müssen von dem C++ RTMP-Server erfüllt werden. Die Möglichkeit des Restreamings^[Restreaming bezeichnet das Einbetten eines Streams auf einer externen nicht autorisierten Seite.] muss verhindert werden, der Server darf nicht von Dritten zum Aufsetzen eines eigenen Streams zugänglich sein und sollte gegen gängige Angriffe aus dem Internet geschützt sein (DOS, MITM, Spoofing, usw.)

Die Beachtung der Breitbandverfügbarkeit spielt eine ebenso wichtige Rolle. Hohe Qualität entsprechend der Anbindung an das Internet wird von den Nutzern vorausgesetzt. Nutzer mit einem schnellen Internetzugang (>5Mbps) erwarten ein hochauflösendes, flüssiges und scharfes Bild, wohingegen Nutzer mit einem langsamen Zugang (Mobiles Breitband - < 1Mbps \ac{DS}) auf entsprechenden Endgeräten ebenfalls ein flüssig laufendes Video mit Abstrichen in der Qualität erwarten. 

Client-Server Architektur
----------------------------------

Die einfache Variante (*Single-Server-Modell*) der Livestreaming Architektur (Abbildung 3.5) besteht aus Akteuren, welche z.B. ein Gruppentraining mit einer HD Kamera filmen. Das Videosignal von der Kamera wird direkt vom Streaming-Client mit laufendem \ac{FMLE} ausgelesen, in unterschiedliche Bitraten konvertiert und nach erfolgreicher Anmeldung an den CRTMP-Server gesendet. Dieser kann je nach Konfiguration die eintreffenden Videostreams als \ac{RTMP}- oder \ac{RTSP}-Stream auf vordefinierten Ports zur Verfügung stellen. Von dem auf der Webanwendung laufenden Flashplayer wird der Videostream dann bei Bedarf auf Client-PCs/Tablets/Smartphones abgespielt.    

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{../figures/live_arch.png}
\caption{Livestreaming Architektur - Einzelner Server}
\end{figure}

Die Architektur zeigt, dass der Server die zentrale Komponente ist und mögliche Einschränkungen von diesem ausgehen.
Je nachdem, wie viele Nutzer sich mit dem Server verbinden, um den Stream zu sehen, werden Hardwareressourcen und vor allem Netzwerkressourcen eingefordert. Um das Problem zu abstrahieren, wird angenommen, dass zum Veröffentlichungszeitpunkt der Anwendung maximal 500 Kunden gleichzeitig einen Livestream verfolgen.
Während sich die Hardwareressourcen auf das Verarbeiten von maximal drei Videostreams beschränken, enthüllt eine einfache Rechnung, dass gewöhnliche Upload-Kapazitäten nicht mehr ausreichen: Geht man von einer durchschnittlichen Bitrate von 1500 kbps pro Client bei 500 Clients aus, muss der Server 500 * 1500 \ac{kbps} = 750 \ac{Mbps} konstant hochladen können.

Was tun bei mehr Nutzern?

Dazu kann ein Master-Server wie im *Single Server Modell* vorhanden mit sogenannten Mirror-Servern zu einem Server-Cluster (Abb. 3.6) verbunden werden, so dass der Master-Server nur Verbindungen von Mirror-Servern zulässt. Jeder Mirror-Server hat dann auch wieder für bis zu 500 Nutzer Kapazitäten. 
Die korrekte Funktionsweise erfordert ein Plugin zum *Load Balancing*. Möchte ein Nutzer den Livestream ansehen, wird zunächst die Latenz zu den jeweiligen Mirror-Servern überprüft und die Last auf den Servern verglichen. Dem Nutzer wird anschließend ein Server zugewiesen. Ist ein Server voll, wird dem Nutzer ein alternativer Server zugewiesen. 

\begin{figure}[H]
\centering
\includegraphics[width=0.7\textwidth]{../figures/live_cluster.png}
\caption{Livestreaming Architektur - Server Cluster}
\end{figure}

\newpage

C++ RTMP Server Konfiguration
----------------------------------

Ein CRTMP-Server wird mit LUA-Skripten konfiguriert (Appendix A und B). Die Hauptkonfigurationsdatei stellt in diesem Fall die *flvplayback.lua* Datei dar. Entscheidend ist neben dem Einstellen von Log-Dateien die Konfiguration der Ports, auf denen der Server mit bestimmten Protokollen kommuniziert. 

Die Firma ViaVitale wird auf dem Client-System den FMLE verwenden. Dieser kommuniziert mit dem RTMP Protokoll. Nachdem eine Anwendung mit einem Namen und einer Beschreibung sowie weiterer optionaler Parameter festgelegt wird, folgt die Definition der Akzeptoren (*acceptors*).
In der *flvplayback.lua* wird Port 1395 für das RTMP Protokoll freigegeben. Das ist der sogenannte *acceptor*. Weitere *acceptors* sind möglich (z.B. für inboundflv, inboundRTSP, inboundRTP, ...) und werden in folgender Form spezifiziert:

    Auszug aus dem Quelltext
    --[[./config/flvplayback.lua]]--
\lstinputlisting[firstnumber=38, firstline=38, lastline=45]{code/flvplayback.lua} 

Anschließend kann der Server noch einen Authentifizierungsmechanismus aktivieren. 
Dafür wird *validateHandshake* auf *true* gesetzt. Gefolgt von einem Authentifizierungsblock.

    Auszug aus dem Quelltext
    --[[./config/flvplayback.lua]]--
\lstinputlisting[firstnumber=46, firstline=46, lastline=55]{code/flvplayback.lua} 

Die in Zeile 55 angegebene *users.lua* Datei enthält die Nutzernamen/Passwort-Kombinationen. In diesem Fall haben nur die beiden Inhaber Thomas und Heike einen eigenen Zugang.

    Auszug aus dem Quelltext
    --[[./config/users.lua]]--
\lstinputlisting{code/users.lua} 

### Sicherheit

Der Server wird auf unterschiedlichen Ebenen abgesichert:

*   Nur Verbindung eines Encoders zum Server wenn Nutzername und Passwort bekannt sind
*   Server validiert IP-Adresse vom Encoding-Client, von weiteren Mirror-Servern und den DN sowie die IP-Adresse der Webandwendung.

Dadurch wird verhindert, dass der Stream von anderen Anwendungen verwendet werden kann. 

Authentifizierung wie zuvor beschrieben schützt davor, dass sich andere Encoder mit dem Server verbinden um diesen selbst zum Streamen zu verwenden.

Validierung von IP-Adressen und Domain-Namen findet beim Verbinden statt. Diese Funktion ist in der Open-Source-Variante des CRTMP-Servers nicht implementiert. Um diese Funktionalität zu erreichen müssen Header und C++ Dateien im *rtmpprtocolhandler* überarbeitet (Appendix C und D) und neu kompiliert werden \cite{Rani:2012:Online}.

### Server-Hardware

Auf einem virtuellen Server des Rechenzentrums (RZ) der Universität Osnabrück (UOS) wurde die Konfiguration des Servers getestet (Tabelle 3.6). Mit der gegebenen Hardware war es möglich bis zu 1000 Verbindungen mit dem Server herzustellen, ohne dass dieser Probleme hatte. Da der Stresstest durch ein vom CRTMP-Server bereitgestelltes Skript auf der Server-Instanz selber ausgeführt wurde, konnten die Upload-Kapazitäten vernachlässigt werden. Die CPU war maximal bis 60% ausgelastet, der RAM bis zu 90%. Die Ergebnisse des Stresstest decken sich auch mit offiziellen Benchmarks \cite{crtmpbench:2010:Online}. Anhand dieser wurde eine Empfehlung ausgegeben. Wenn entsprechende Upload-Kapazitäten verfügbar sind wäre es mit neuester Hardware (Intel i5 Ivy-Bridge CPU, >= 16 GB RAM) möglich bis zu 12000 Nutzer pro Instanz zu verwalten \cite{evostream:2013:Online}. Dafür müsste der Server über mindestens 40 Gbps Upstream verfügen. 
\newpage

: Hardwareübersicht Testsystem und Empfehlung

+-------------------------+-------------------------------------+
|    Testserver RZ-UOS    |             Empfohlen               |
+=========================+=====================================+
| Virtuelle Serverinstanz | Dedizierte Serverinstanz            |
+-------------------------+-------------------------------------+
| Ubuntu 12.04 64 Bit     | Free BSD                            |
+-------------------------+-------------------------------------+
| Intel Xenon @ 2.4GHz    | Intel Core 2 Duo E8400 @ 3.00GHz oder besser    |
+-------------------------+-------------------------------------+
| 2 GB RAM                | 4 GB RAM oder mehr                             |
+-------------------------+-------------------------------------+
| k.A.                    | 1,5 GB Upstream^[*Unter der Annahme, dass die durchschnittlich zu übertragene Bitrate pro Nutzer (bei maximal 500 Nutzern) 1500 Kbps beträgt.*]               |
+-------------------------+-------------------------------------+

 


Flash Media Live Encoder 
---------------------------------------

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{../figures/FMLE_Config.png}
\caption{Flash Media Live Encoder - Konfiguration}
\end{figure}

Der \ac{FMLE} erlaubt es in Echtzeit Audio und Video-Eingänge per RTMP an den \ac{CRTMPS} zu senden. 

Diverse Konfigurationsmöglichkeiten stehen zur Verfügung. Audio und Videoquelle werden zunächst ausgewählt. Es werden interne sowie externe Geräte unterstützt. H.264 (MPEG-4 - Part 10) bzw. VP6 können zur Videokompression verwendet werden. Zudem kann die Bildwiederholrate und Auflösung festgelegt werden. FMLE erlaubt es zeitgleich ein bis drei Streams mit unterschiedlichen Bitraten und Auflösungen an den C++ RTMP Server zu senden. Dadurch können Nutzer mit höherer sowie niedrigerer Breitbandverfügbarkeit mit entsprechender Qualität erreicht werden. 

Für das Audiosignal steht neben dem MP3-Format auch das NellyMoser-
Format bereit. Es werden Mono- und Stereochanneling bei "Sample-Raten" von 11025/22050 bzw. 44100 unterstützt. Mögliche Bitraten liegen zwischen 96 und 224 Kbps. 

Auf der rechten Seite der GUI werden die Serverdaten eingetragen. Neben dem Hauptserver kann noch eine Backup-URL sowie der Stream-Name angegeben werden. Diverse Parameter stehen zur Differenzierung bereit.

Parameter: 

*   %i - Index der Bitrate
*   %v - Video Bitrate
*   %a - Audio Bitrate
*   %b - %v + %a
*   %f - Framerate

Diese Parameter können auch beim lokalen Abspeichern des Stream genutzt werden, um den Videodateien ausdrucksstärkere Namen zu geben.

### Konfiguration

Für die Firma ViaVitale wurde ein Profil erstellt und gespeichert, welches wichtige Anforderungen erfüllt.

Zur Videokompression wird der H.264 Codec genutzt. Eine Bildwiederholrate von 25 liefert ein flüssig laufendes Video.
Es werden drei Streams mit unterschiedlichen Auflösungen und Bitraten erzeugt. *Stream 1* hat eine Auflösung von 640 * 360 bei 650 Kbps, *Stream 2* läuft mit 960 * 540 bei 1200 Kbps und *Stream 3* hat eine Auflösung von 1280 * 720 bei 2000 Kbps. Das Audiosignal wird bei einer *Sample Rate* von 44100 Hz und einer Bitrate von 128 Kbps Mp3 komprimiert. 

Zum Streamen dieser Konfiguration werden demnach mindestens 4234 Kbps Upstream benötigt. 

Um Spikes und Schwankungen im Upstream auszugleichen sollte daher eine Upload Anbindung von mindestens 9Mbps vorliegen. Führende Livestreamingportale wie Livestream.org bzw. Ustream.com geben für den Broadcaster als Empfehlung eine Auslastung der Uploadgeschwindigkeit von 40-50% des Maximums an \cite{Ustream:2013:Online} \cite{Livestream:2013:Online}. 

### Hardware

Neben der zuvor angesprochenen Anbindung an das Breitbandnetz muss der Broadcaster-PC über eine ausreichend gute Hardware verfügen, um die einzelnen Frames zu cachen und schnell genug komprimieren zu können, so dass die Bildwiederholrate konstant bleibt.

Livestream.org empfiehlt mindestens eine Intel i7-Quad Core CPU zu verwenden, 8 GB RAM zu verbauen und eine dedizierte Videokarte mit mindestens 512 MB Speicher zu verwenden. Als Betriebssystem sollte Windows 7 oder 8 verwendet werden \cite{Livestream:2013:Online}.

Flowplayer
-----------------------------------------

Um den Livestream auf dem Endgerät des Nutzers abspielen zu können, wird in der Webanwendung der Flowplayer verwendet. Dieser ist ein HTML5-Player mit Flash-Fallback Funktionalität. Der Flowplayer ist *responsive* und das Design mit CSS anpassbar (eigenes Logo - kostenpflichtige Version). Weitere Funktionen lassen sich per Javascript ergänzen. 

Mit einer Gesamtgröße von 61kB (exkl. jQuery) (JavaScript 34kB, Flash 5kB, CSS 22kB (+ jQuery 92kB)) müssen nur geringe Datenmengen nutzerseitig gecached werden.

Das Plugin wird per Javascript in den \ac{DOM}-Tree geladen. In dem Aufruf wird der Pfad zum Flash-Player und RTMP-Stream angegeben (Appendix F). 
Weitere Konfigurationsmöglichkeiten stehen zur Verfügung (z.B. ein Splash-Screen). 

    Auszug aus dem Quelltext
    /*
     * app / assets / javascripts / livestreams.js 
     */
\lstinputlisting[firstnumber=37, firstline=37, lastline=45]{code/livestreams.js} 
    
    ...
\lstinputlisting[firstnumber=56, firstline=56, lastline=56]{code/livestreams.js} 

Im HTML-Code des Frontends muss anschließend noch ein *div* mit entsprechender Klasse und integriertem Video-Tag mit der Source-URL angegeben werden (Appendix E).
    
    Auszug aus dem Quelltext
    /*
     * app / views / livestreams / _index.html.haml
     */
\lstinputlisting[firstnumber=6, firstline=6, lastline=10]{code/index.html.haml} 
