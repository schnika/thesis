ViaVitale
==================================

ViaVitale^[Der Internetauftritt ist unter www.pilatesaufmallorca.com zu finden.] ist eine Firma mit Sitz auf Mallorca, die 2002 von Heike und Thomas Niemeier gegründet wurde. Beide sind ausgebildete Trainer im Bereich Rückenpilates, Personaltraining und Kinderboxen. 

Konzept
=============================

Die Firma ViaVitale hat ein komplexes Konzept zur Gründung des ersten Online-Rückenstudios vorgestellt. 
Das Kernprodukt soll eine Social-Media-Network-Webanwendung sein, die eine besondere Nische in dem stark wachsenden Online-Fitnessstudio-Segment ausfüllt.

Neben klassischen Rückenübungen soll ein besonderer Schwerpunkt auf Yoga und Pilates in enger Verbindung zur zugrunde liegenden Philosophie liegen. D.h. Muskelaufbau, Kräftigung und Stabilisierung des Körpers wechseln mit körperlicher und geistiger Entspannung sowie mentalen Übungen. 

Die Webanwendung soll kostenpflichtige und kostenfreie Inhalte bieten. Eine "SOS-Seite" wird Nutzern bei akuten Problemen schnelle Hilfe bieten. Ärzte, Trainer und Physiotherapeuten veröffentlichen zukünftig in einem Blog regelmäßig Artikel zum Thema Rücken und Fitness. 
Kurze Trainingsvideos und Beispieltrainingspläne geben zudem einen Einblick in die Vorteile des "Abo-Kontos". Ein Abonnement soll monatlich EUR 12,95 kosten und jederzeit kündbar sein. 

Wer sich für ein Abo entscheidet, erhält neben allen sozialen Funktionen (Rückenfreunde finden, Trainingspläne, Erfolge, Freundschaften und Termine teilen) auch Zugang zu regelmäßig ausgestrahlten Livesendungen und einer Vielzahl an aufgezeichneten Trainingsvideos. Ein Erfolgssystem soll die Kundenbindung erhöhen und zu einer längeren Mitgliedschaft beitragen.

Eine eigene Produktlinie soll über den *Online-Shop* vertrieben werden. Dort können Nutzer auch "Mobile-Apps" oder Trainingswochenenden auf Mallorca direkt kaufen. Wer sich eine persönlichere Betreuung beim Training wünscht, kann hier zudem Stunden bei einem ausgebildeten Trainer buchen. 

Ergänzend zur Webanwendung ist die Entwicklung einer App für IOS und Android Tablets und Smartphones geplant, welche es Nutzern ermöglicht auch mobil auf alle Inhalte zugreifen zu können. 

Um die Zielgruppe zu erweitern, möchte die Firma ViaVitale auch an Firmen herantreten und eine eigene Desktopanwendung anbieten. Diese soll in regelmäßigen Abständen zu kurzen Pausen motivieren, in denen Übungen zur Haltungsverbesserung oder Entspannung durchgeführt werden.

Alle Anwendungen sind auf neueste Technologien abzustimmen, um ein zeitgemäßes Produkt auf den Markt zu bringen.

Team
------------------------------------

Für die Realisierung dieses Konzepts setzt sich das Team wie in Tabelle 2.1 aufgeführt zusammen.

: Backzoom ProjektmitarbeiterInnen (Stand: April 2013)

+-------------------------+--------------------------------------------------------------------+
|          Name           |                            Funktion                                |
+=========================+====================================================================+
| Heike & Thomas Niemeier | Inhaber, Gesamtkonzept, Gesamtleitung und Gesamtkoordination       |
+-------------------------+--------------------------------------------------------------------+
| Réne & Sascha Gerritsen | Start-Up Webentwickler (nerd-brothers.de), Leitung Webanwendung    |
+-------------------------+--------------------------------------------------------------------+
| Patrick Schnetger       | Konzept und Entwicklung Video-Komponente                           |
+-------------------------+--------------------------------------------------------------------+
| Henning Strüber         | Konzept und Entwicklung Social-Network-Komponente                  |
+-------------------------+--------------------------------------------------------------------+
| Jürgen Jaskoviak        | Webdesigner                                                        |
+-------------------------+--------------------------------------------------------------------+
| Video & Tontechniker    | Produktion der Video-on-Demand- und Live-Inhalte                   |
+-------------------------+--------------------------------------------------------------------+
| Marketingfirma          | Bewerben der Plattform zum Release                                 |
+-------------------------+--------------------------------------------------------------------+

Zielsetzung für das Teilprojekt Video-Komponente
=============================

Die Video-Komponente besteht aus zwei Teilen: Livestreaming und Video-on-Demand.

Aufgabe der vorliegenden Bachelorarbeit war es, diese Komponenten unter Berücksichtigung der Kompatibilität der zur Zeit gängigen Endgeräte und unter Beachtung der Breitbandverfügbarkeit zu entwickeln.

Livestreaming
---------------------------------

Speziell gilt für die Livestreaming-Komponente, eine Empfehlung für eine geeignete Client- und Serverarchitektur zu liefern. Dazu wird für alle beteiligten Systeme (Streaming-Client, Streaming-Server und Webanwendung) geeignete Hardware vorgestellt und eine sinnvolle Konfiguration der Software vorgeschlagen. 
Für die Entwicklung und Einbindung soll so weit wie möglich auf Open-Source-Software zurückgegriffen werden.
Ergänzend sollen Konzepte und eigene Beispiele zur Sicherheit des Streaming-Servers vorgestellt werden. Dazu gehört die serverseitige Validierung des Streaming-Clients, ein Authentifizierungsmechanismus des Clients zum Server und die Validierung der Anwendung, auf dem der Stream letztendlich abgespielt wird.

Video-on-Demand
---------------------------------

Für die \ac{VoD}-Komponente besteht das Ziel darin, dem Nutzer aufgezeichnete Livesendungen bzw. Trainingsvideos unter der Verwendung aktueller Technologien zur Verfügung zu stellen. Eine Empfehlung zur Absicherung der Videoinhalte soll ebenfalls vorgestellt werden. Wie beim Livestreaming dürfen Videos nur von der Webseite selbst oder den zugehörigen Apps abgespielt werden.

Verwendete Technologien
====================================

Die Webanwendung wird mit Rails entwickelt. Rails ist ein in Ruby geschriebenes Framework um agile Webanwendungen zu entwickeln. Rails verfügt über eine große Anzahl von fertigen Modulen (so genannte *gems*), die den Entwicklungsprozess in allen Phasen unterstützen sollen. Das Rails-Framework verfolgt das Model-View-Controller-Konzept, sowie die Programmierparadigmen *Don't Repeat Yourself*, *Convention Over Configuration* und *Representational State Transfer (REST)* \cite{Hartl:2012}. Für die Video-Komponente sind zwei *gems* besonders hervorzuheben: *Paperclip* und *aws-sdk*. 

*Paperclip* ist eine Bibliothek, die ActiveRecord^[ActiveRecord ist eine Ruby Bibliothek, welche ORM (Object-Relational-Mapping) implementiert.] um Funktionalitäten zum Hochladen und Speichern von Dateien bereitstellt. *Paperclip* Objekten werden beim Erzeugen spezielle Optionen zugewiesen werden. Beispielsweise lassen sich für hochgeladene Bilder *Styles* definieren. Ein Preprocessor wertet diese *Styles* aus und wandelt Bilder entsprechend in andere Bild- oder Containerformate um. In Zusammenarbeit mit *FFmpeg* (Kapitel 3.3.5) lassen sich auch Videos in andere Formate, Codecs und Auflösungen mit unterschiedlichen Bitraten konvertieren. 

Mit Hilfe des *aws-sdk*-Gems wird die Kommunikation mit \ac{AWS} vereinfacht. *Aws-sdk* ist eine Bibliothek und neben Ruby auch für *PHP*, *Java* oder *Python* verfügbar. In Kombination mit *Paperclip* lassen sich Videos oder Bilder sicher auf Amazon Servern speichern (s. Amazon S3 Kapitel 3.6.1).

Ajax (Asynchronous JavaScript and XML) ist die zugrunde liegende Technologie für eine *Single Page Application*. Ajax ermöglicht es, Daten asynchron zwischen dem Server und dem Client auszutauschen. Das bedeutet, dass die Anwendungslogik auf das Client-System ausgelagert wird. Die Webseite muss nur noch einmal geladen werden. Lediglich in vordefinierten Bereichen werden Inhalte nach Bedarf über HTTP-Anfragen geladen.

Um alle Funktionen zu präsentieren wurde das CSS-Framework metroUI verwendet. Ein CSS-Framework beinhaltet fertige Designs für alle wichtigen HTML-Elemente wie *header*, *body*, *footer*, *table*, *ul/li*, usw.

Für ein dynamischeres Nutzererlebnis macht die Anwendung von der PushServer-Technologie Gebrauch. Eine herkömmliche Server/Client-Verbindung ist unidirektional und erfordert Nutzereingaben. Bei der PushServer-Technologie wird eine bidirektionale Verbindung aufgebaut, so dass der Server Funktionen clientseitig ausführen kann. Schickt *Nutzer A* beispielsweise eine Nachricht an *Nutzer B*, würde *B* bei einer unidirektionalen Verbindung erst beim Aktualisieren der Anwendung die neue Nachricht im Posteingang wahrnehmen. In der bidirektionalen Verbindung sorgt der Server dafür, dass bei *B* der Posteingang aktualisiert und das Empfangsevent sofort angezeigt wird.

Des Weiteren kommen bei der Video-Komponente *\ac{CRTMPS}* (Kapitel 3.3.3), *\ac{AWS}*(Kapitel 3.6.1), *\ac{FMLE}*(Kapitel 3.5.4) und der *Flowplayer*(Kapitel 3.5.5) zum Einsatz. Tabelle 2.2 fasst die eingesetzten Technologien zusammen.        

\newpage

: Verwendete Technologien

+--------------------------+--------------------------------------------------------------------------------------------------------------------+
|           Name           |                                                       Funktion                                                     |
+==========================+====================================================================================================================+
| Ruby on Rails            | Framework zum Entwickeln agiler   Webanwendungen                                                                   |
+--------------------------+--------------------------------------------------------------------------------------------------------------------+
| Ajax                     | Javascript-Bibliothek                                                                                              |
+--------------------------+--------------------------------------------------------------------------------------------------------------------+
| PushServer               | Bidirektionale Client-Server Verbindung. Ermöglicht Server-seitig induziertes Ausführen von Funktionen beim Client |
+--------------------------+--------------------------------------------------------------------------------------------------------------------+
| C++ RTMP Server          | Open Source Livestreaming Server; in C++ geschrieben                                                               |
+--------------------------+--------------------------------------------------------------------------------------------------------------------+
| Amazon Web Services      | Kostenpflichtige Dienste z.B. zum Speichern und Verbreiten großer Datenmengen (Content Delivery Network)           |
+--------------------------+--------------------------------------------------------------------------------------------------------------------+
| Flash Media Live Encoder | Encoding-Software von Adobe                                                                                        |
+--------------------------+--------------------------------------------------------------------------------------------------------------------+
| metorUI                  | CSS-Framework zum Gestalten der Demo-Anwendung                                                                     |
+--------------------------+--------------------------------------------------------------------------------------------------------------------+
| Flowplayer               | HTML5 bzw. Flash-Player                                                                                            |
+--------------------------+--------------------------------------------------------------------------------------------------------------------+
| Git                      | Versionskontrolle                                                                                                  |
+--------------------------+--------------------------------------------------------------------------------------------------------------------+