Breitbandverfügbarkeit
=================================

Das Bundesministerium für Wirtschaft und Technologie (BMWi) veröffentlicht in regelmäßigen Abständen Informationen zur \ac{BBV} in Deutschland. Die BBV und Verbreitung der unterschiedlichen Technologien muss bei der Konzipierung der Video-Komponente einbezogen werden. 

Neben gängigen Technologien wie (V)DSL, LWL (Glasfaser, Lichtwellenleiter), UMTS/HSPA, WLAN und Satellit werden auch neue Technologien wie LTE^[Long Term Evolution Standard 3.9G gehört zu der vierten Mobilfunktechnik-Generation und ermöglicht bis zu 300 Mbps \ac{DS} und 75 Mbps \ac{US} \cite{GUTT:2010} im mobilen Netz] begutachtet \cite{BMWi:2012}. 

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{../figures/LTE_2012.png}
\caption{Entwicklung der LTE-Verfügbarkeit in Deutschland \cite{BMWi:2012} }
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{../figures/BBV_2012.png}
\caption{Entwicklung der \ac{BBV} in Deutschland - alle Technologien \cite{BMWi:2012}}
\end{figure}

Nach einer Untersuchung des TÜV Rheinland in Zusammenarbeit mit dem \ac{BMWi} ist LTE bereits in über 50% der Haushalte in Deutschland verfügbar - Mitte 2011 waren es erst ca. 10% (Abb. 3.1). Auch wenn mit LTE Datenraten von bis zu 300 \ac{Mbps} \ac{DS} möglich sind, gibt es häufig Einschränkungen in der Geschwindigkeit und im monatlichen Datenvolumen durch die Provider. Führende Mobilfunkkonzerne begrenzen die Datenraten auf 3 - 7 \ac{Mbps} und beim Überschreiten einer gewissen Datenmenge (15/20/50 GB/Monat) sogar auf 384 \ac{kbps} [Quelle: http://injelea-blog.de/2012/10/02/das-duell-lte-statt-dsl/](http://injelea-blog.de/2012/10/02/das-duell-lte-statt-dsl/). Zudem ist das LTE-Netz im Gegensatz zu einem DSL-Anschluss nicht "exklusiv". Das bedeutet, dass sich je nach Auslastung in der Umgebung zusätzliche Einschränkungen in der Geschwindigkeit bemerkbar machen können. Das spiegelt sich auch in einem Bericht der Firma Akamai wieder, in dem für das mobile Breitbandnetz durchschnittliche Downloadraten von 1,3 - 5,7 \ac{Mbps} (je nach Provider) im 1. Quartal 2013 aufgezeichnet wurden \cite{Belson:2013}.
Betrachtet man die BBV für alle Technologien (Abb. 3.2) sind bereits über 99,3 % der Haushalte mit einer Anbindung >= 2 \ac{Mbps} ausgestattet. Über 6 \ac{Mbps} \ac{DS} ist für über 90% der Haushalte zugänglich.  

Das \ac{BMWi} gibt zudem in seinem dritten Monitoring-Bericht zur Breitbandstrategie vor, dass bis zum Jahr 2014 über 75% der Haushalte 50 \ac{Mbps} Breitband zur Verfügung stehen soll. Bis 2018 soll dieses Ziel flächendeckend erfüllt werden. 

Das bedeutet für die potentiellen Nutzer, dass neben Videos in hoher Auflösung und Qualität (High Definition - ab 2 \ac{Mbps}) auch Videos in geringerer Qualität verfügbar sein müssen. Das liegt auch an der weiter steigenden Anzahl mobiler Internetnutzer, die jedoch zur Zeit mit den genannten Restriktionen durch Ihre Vertragspartner rechnen müssen. 

Streaming Protokolle
==================================

Um Live- und On-demand- Videos an den Nutzer zu bringen, gibt es verschiedenste Streaming-Protokolle. In der Vergangenheit waren die Protokolle RTMP und RTSP die gängigen. In den letzten Jahren wurde vermehrt auf verschiedene HTTP-Protokolle gesetzt, um Videoinhalte 
bereitzustellen. Der Grundgedanke dahinter: "Warum sekundäre Protokolle verwenden, anstatt auf das Protokoll zu setzen, auf dem das Internet gegründet wurde" \cite{Zambelli:2013}?  

RTMP und RTSP wurden speziell entwickelt, um Video- bzw. Audiodateien über das Internet zu verbreiten. Das RTMP-Protokoll wurde von Adobe entwickelt und ist ein reines Flash-Protokoll. Ein Media Server kann mit Hilfe dieses Protokolls Daten an einen Flash Player senden. RTMP verwendet standardmäßig den TCP/IP-Port 1935 für die Kommunikation. Es gibt unterschiedliche Varianten. RTMPT (*tunneled*) nutzt das HTTP Protokoll für die Kommunikation um z.B. Firewalls zu umgehen, und RTMPS (*secure*) überträgt die Daten per HTTPS. Da Adobe die Unterstützung für den mobilen Sektor eingestellt hat \cite{Winokur:2011:Online}, ist dieses Protokoll weder auf IOS noch auf Android-Systemen lauffähig. Für den Desktopbereich scheint RTMP aber immer noch die erste Wahl zu sein. Das bestätigt sich durch die Unterstützung des Protokolls fast aller Open-Source- und kommerzieller Media-Server. 

RTSP ist ein Standard und wurde von der IEFT^[Die *Internet Engineering Task Force* ist eine offene internationale Gemeinschaft. Eine Gemeinschaft aus Wissenschaftlern, Designern, Verkäufern und Unternehmern haben sich zum Ziel gesetzt das Internet durch das Verfassen von Standards zu verbessern.] entwickelt. Daten werden per UDP bzw. TCP als Unicastdatenstrom übertragen. Genau wie beim RTMP-Protokoll handelt es sich bei RTSP um ein "Echtzeitprotokoll". In dem Moment, wo der Client Inhalte vom Server anfragt, werden diese als Paketstrom an den Client gesendet. Die Unterstützung der Endgeräte hat in den vergangenen Jahren immer weiter abgenommen. Das kann vor allem durch die vorrangige Entwicklung HTTP-basierter Dienste begründet werden. Nutzer können einen RTSP-Stream durch die Verwendung von Quicktime-, RealPlayer- oder VLC-Plugins abspielen. Verbreitete Browser wie Chrome oder IE unterstützen RTSP nicht. IOS-Endgeräte unterstützen RTSP nativ nicht, Android-Endgeräte ab Version 3.0 können RTSP-Streams empfangen \cite{Android:2013:Online}. 

Wie eingangs des Kapitels erwähnt wird die Verbreitung von Video- und Audioinhalten per HTTP vorangetrieben. Der größte Unterschied zwischen HTTP-Streaming und Streaming mit klassischen Protokollen (RTSP/RTMP) ist die Art und Weise, wie die Inhalte vom Server zum Nutzer kommen. Bei RTMP und RTSP fordert der Nutzer eine Datei an und diese wird direkt in kleinen Paketen gesendet. Beim HTTP-Streaming wird eine Videodatei oder ein Livestream zunächst auf dem Server gecached und es wird eine *Playlist* erstellt, die vom Nutzer abgefragt wird (*.f4m* HDS bzw *.m3u8* bei HLS). Die *Playlist* enthält die Informationen über die Reihenfolge der erstellten Dateien mit den jeweiligen Bitraten. Je nach Verfügbarkeit kann dynamisch die Qualität (Bitrate) angepasst werden. Das wird als Adaptive Bitrate Streaming^[Adaptive Bitrate Streaming bedeutet, dass sich die Bitrate, mit der das Video übertragen wird selbstständig an die Kapazitäten des Nutzer anpasst.] bezeichnet. Durch das Cachen ist die Zeitverzögerung beim Streamen etwas größer. Des weiteren ist es möglich neben den Funktionen Start und Stopp das Video zu verlangsamen, vor- oder zurückzuspulen.

Adobe hat mit dem ac{HDS} ein eigenes HTTP-basiertes Protokoll veröffentlicht. Es können Live- und On-demand-Inhalte verbreitet werden. Authentifizierung und Verschlüsselung kann mit Hilfe eines Adobe Access Servers geregelt werden. HDS unterstützt die Video-Codes: VP6/MP3 und H.264/AAC \cite{Adobe:2013}. HDS-Inhalte können von Android-Endgeräten nicht abgespielt werden, da das Playlisten-Format *.f4m* nicht unterstützt wird. 

Apple hat ein eigenes HTTP-basiertes Protokoll namens ac{HLS} entwickelt. Wie bei HDS wird Authentifizierung und Verschlüsselung unterstützt \cite{Apple:2013:Online}. Alle IOS-Endgeräte ab Version 3.0 können *.m3u8* Playlisten abspielen. Android-Geräte können das ab Version 4.0 \cite{Android:2013:Online}.

Media-Server Optionen
==================================

Auf dem Markt gibt es aktuell eine Vielzahl von Media-Servern. Die größte Herausforderung - mit einer Software alle Endgeräte zu erreichen - wird derzeit nur von kommerziellen Anbietern gelöst. Trotzdem bieten auch Open-Source-Lösungen neben guter Dokumentation und einer großen Entwicklergemeinde einen schnellen Einstieg mit einer relativ großen Abdeckung der Endgeräte. Wer auf die neuen Technologien wie HDS oder HLS setzen möchte, die bewährten Technologien RTMP und RTSP aber nicht vernachlässigen will, kommt um eine kostenpflichtige Lösung aber kaum herum. Wer voll auf Open-Source setzen möchte, muss die entsprechenden Module selber entwickeln und die Software muss neu kompiliert werden.
Im folgenden werden zwei kommerzielle und drei Open-Source Produkte vorgestellt.

Adobe Media Server
----------------------------------

Der \ac{AMS} wird von Adobe entwickelt und vertrieben - aktuell in der Version 5. Der Server kann Inhalte via RTMP, HDS, HSL und RTMFP^[Das Real Time Media Flow Protokoll ist ein vom Client "getragenes" Kommunikationsprotokoll. Der Adobe Flash Player ab Version 10 und Adobe AIR ab Version 1.5 unterstützen dieses Protokoll. Es wird häufig in Webcam Chaträumen, VoIP, Live-Kundensupport oder Onlinespielen verwendet \cite{Larson-Kelley:2012}.] verbreiten. Durch die Wahl der Kommunikationsprotokolle können Live- und On-demand-Inhalte an Desktops, Hybrid-Fernseher, Tablets und Smartphones inklusive iOS- und Android-Devices gestreamt und geschützt werden. Als Hostsystem eignen sich Windows Server 2008 RC2, Red Hat Enterprise Linux Server 5.5 (64 Bit) oder Linux CentOS 5.8 (64 Bit) \cite{Larson-Kelley:2012}.

Für Entwickler gibt es eine Starter-Edition, die eine Beschränkung auf die Anzahl der zulässigen Verbindungen hat. Für ein fertiges Produkt stehen Standard, Professional und Expert-Editionen bereit \cite{Larson-Kelley:2012}. Fertig aufgesetzte Serverinstanzen können direkt über Amazon EC2-Instanzen angemietet werden. 

Durch eine Zusammenarbeit mit Amazon ist der \ac{AMS} relativ stark verbreitet. Es gibt vorkonfigurierte AMS Amazon EC2-Instanzen und alle Amazon CloudFront-Instanzen  (s. Kapitel 3.6.1) verwenden den \ac{AMS} in der Version 3.5. 

Wowza Media Server
----------------------------------

Der Media Server der Firma Wowza Media Systems wird in der Version 3.6 kommerziell vertrieben. Die folgenden Informationen wurden der aktuellen Produktspezifikation entnommen \cite{Wowza:2013}. Der Media-Server ermöglicht es, Live- und On-demand-Inhalte mit Hilfe der vorgestellten Streaming-Protokolle (RTMP, HDS, HLS, RTSP/RTP, MPEG-TS, and Smooth Streaming) bereitzustellen. Auf Nutzerseite können die erzeugten Streams von folgenden Geräten bzw. folgender Software abgespielt werden: Adobe Flash-Player, Microsoft Silverlight-Player, Apples' iPhone, iPad und iPod-Touch, QuickTime-Player, Android Smartphones und Tablets sowie einer Vielzahl mit dem Internet verbundener Fernseher und IPTV/OTT Set-Top-Boxen (Abb. 3.3). Die Software ist darauf ausgelegt, sowohl auf einem einzelnen Server als auch in einem Server-Cluster inklusive *Load-Balancing* zu arbeiten. Durch die Aktivierung eines AddOns kann *Digital Rights Managment* (DRM) für die Inhalte konfiguriert werden.

Da die Software mit Java entwickelt wurde, muss das *Java Runtime Environment* (JRE) oder *Java Development Kit* (JDK) in der Version 6 oder größer auf dem Host-System installiert sein. Die Verwendung von Java als Programmiersprache macht das Produkt plattformunabhängig (lauffähig unter Windows (XP, Vista, 7, Server 2003, 2008, 2012), Linux (alle Distributionen), Solaris, Mac OS und Unix).

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{../figures/Wowza_Device_overview.png}
\caption{Wowza Media Server v3.6 Architektur Übersicht. \cite{Wowza:2013}}
\end{figure}

Eine Entwickler-Lizenz wird kostenlos bereitgestellt und ermöglicht es, die jeweiligen Dienste für bis zu neun gleichzeitige Verbindungen zum Server zu entwickeln und zu testen. Eine normale Lizenz ist für 30 Tage kostenlos und umfasst alle Funktionen. Wird die Software weiter verwendet, können Tages-/Monats- oder Lebenszeitlizenzen erworben werden.

Der Wowza Media Server wird u.a. von folgenden Unternehmen verwendet:

*   livestream.org
*   justin.tv
*   TVW
*   livstation
*   Sverige Radio (Radio Sweden)

C++ RTMP Server
----------------------------------

Wie der Name verrät ist der Server in C++ geschrieben. Der C++ RTMP Server ist eine Open-Source-Software, um Audio oder Video aufzunehmen bzw. zu senden. CRTMP-Server unterstützt diverse Technologien. Der Server ist in der Lage Flash zu empfangen und zu senden (RTMP,RTMPE, RTMPS, RTMPT, RTMPTE), Signale von eingebetteten Geräten (Android, IP-Kameras, Hardware-Encoders) zu empfangen und zu senden, Videos von IOS-Geräten zu empfangen und via MPEG-TS, RTSP/RTP zu senden. Die Auswahl der Protokolle schließt die Erreichbarkeit von IOS-Geräten aus. Es wäre möglich ein HLS-Plugin für den \ac{CRTMPS} zu entwickeln - wie es bei einer kommerziellen Version, Evostream Media Server, der Fall ist \cite{Eugen-Andrei:2013:Online} . 

Red5 Server
----------------------------------

Der Open-Source Red5-Server wurde mit Java entwickelt. Mit Hilfe des Servers ist es möglich, Flash-Inhalte Live und On-demand zu streamen. Red5 setzt lediglich auf die Protokolle RTMP/T /S und RTMPE \cite{Red5:2013:Online}. Durch diese Einschränkung ist es nur möglich, Endgeräte mit einem lauffähigen Flash-Player zu erreichen. IOS-Geräte und einige Android-Geräte sind davon betroffen. Daher wird der Red5-Server ausschließlich im Desktop-Bereich eingesetzt. Es wäre allerdings möglich einen Red5 RTMP-Stream mit Hilfe von FFmpeg in einen HLS-Stream zu konvertieren. 

FFmpeg
----------------------------------

FFmpeg ist ein plattformunabhängiges Kommandozeilenprogramm, um Video- und Audiodateien unterschiedlicher Formate aufzunehmen, zu konvertieren oder zu streamen. Zum streamen stehen u.a. die Protokolle RTMP, RTMPE, RTMPS, RTMPT, RTMPTE, HLS und RTSP/RTP bereit \cite{FFmpeg:2013:Online}. Der Fokus von FFmpeg liegt aber nicht im Streaming-Bereich sondern eher im Bereich der En- bzw. Dekodierung von Audio und Video (Eine vollständige Liste aller unterstützen Datei-, Bild-, Audio-, und Videoformate ist unter *http://ffmpeg.org/general.html* zu finden).  

FFmpeg wird in der VoD-Komponente verwendet und kommt beim *Preprocessing* (Kapitel 3.6.3) zum Einsatz.

Um beispielsweise ein Video H.264-Format (mpeg-4) mit konstanter Bitrate zu konvertieren, wird der folgende Befehl ausgeführt:

``ffmpeg -i input -c:v libx264 -b:v 2500k -minrate 2200k -maxrate 2800k out.mp4``

*-i* input gibt den Pfad zur Videodatei an. *-c:v* steht für den Codec. *-b:v* für die durchschnittliche konstante Bitrate. Schwankungen werden durch -minrate und -maxrate ausgeglichen. *-out* gibt den Pfad zur Ausgabedatei an. Zusätzliche Parameter zum Setzen des Audiocodecs sind vorgesehen. 


Videoformate
==================================

Bei der Wahl des Videoformates beim Livestreaming sowie \ac{VoD} müssen nicht nur die Protokolle, sondern auch die gängigen Browser, mit denen die Webanwendung aufgerufen wird, berücksichtigt werden. Nativ werden auf den Systemen IOS, Android bzw. Windows nicht alle Formate gleichermaßen unterstützt. Die Entwicklerfirmen der Browser haben in den vergangenen Jahren auf unterschiedliche Technologien für Audio und Video in ihren Browsern gesetzt und die Unterstützung anderer Formate vernachlässigt. In Tabelle 3.1 ist die Browsernutzung in Europa zu sehen. 

: Browserverteilung in Europa (Juli 2012 - Juli 2013)

+-------------------------+--------+
|         Browser         | Anteil |
+=========================+========+
| Internet Explorer 8.0-  | ca. 8% |
+-------------------------+--------+
| Internet Explorer 9.0+  | 15,7%  |
+-------------------------+--------+
| Chrome(alle)            | 34,6%  |
+-------------------------+--------+
| Firefox 5+              | 27,6%  |
+-------------------------+--------+
| Safari 5.0+             | 4,63%  |
+-------------------------+--------+
| Safari iPad             | 3,42%  |
+-------------------------+--------+
| Andere                  | ca. 6% |
+-------------------------+--------+


Allein für die Android-Plattform sind nach dem Stand von August 2012 6 verschiedene Versionen vorhanden wie Abbildung 3.4 zeigt \cite{Android:2013:Online}.

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{../figures/android_version_distribution_Aug_2013.png}
\caption{Android Platform Versionsverteilung 1. August 2012 \cite{Android:2013:Online}}
\end{figure}

: HTML5 Videoelement Unterstützung Desktop-Browser (Codec: WebM/VP8, MPEG-4/H.264, Ogg/Theora)(Stand: Juli 2013) \cite{CanIuse:2013:Online}

+--------------+--------------+---------------+--------------+--------------+-------------+
|   Version    |      IE      |     Firefox   |     Chrome   |    Safari    |    Opera    |
+==============+==============+===============+==============+==============+=============+
| 3.Vorgänger- | 7.0: Nein    | 20.0: Ja      | 26.0: Ja     | 4.0: Ja      | 11.6: Ja    |
| version      |              |               |              |              |             |
+--------------+--------------+---------------+--------------+--------------+-------------+
| 2.Vorgänger- | 8.0: Nein    | 21.0: Ja      | 27.0: Ja     | 5.0: Ja      | 12.0: Ja    |
| version      |              |               |              |              |             |
+--------------+--------------+---------------+--------------+--------------+-------------+
| Vorherige    | 9.0: Ja      | 22.0: Ja      | 28.0: Ja     | 5.1: Ja      | 12.1: Ja    |
| Version      |              |               |              |              |             |
+--------------+--------------+---------------+--------------+--------------+-------------+
| Aktuelle     | 10.0: Ja     | 23.0: Ja      | 29.0: Ja     | 6.0: Ja      | 15.0: Ja    |
| Version      |              |               |              |              |             |
+--------------+--------------+---------------+--------------+--------------+-------------+
| Nächste      | 11.0: Ja     | 24.0: Ja      | 30.0: Ja     | 7.0: Ja      | 16.0: Ja    |
| Version      |              |               |              |              |             |
+--------------+--------------+---------------+--------------+--------------+-------------+

\newpage

: HTML5 Videoelement Unterstützung IOS/Android/Mobile-Browser (Codec: WebM/VP8, MPEG-4/H.264, Ogg/Theora)(Stand: Juli 2013)\cite{CanIuse:2013:Online}

+---------------+---------------+-------------+-------------+----------------+-----------------+
|    Version    |  iOS Safari   |   Android   |    Opera    |   Chrome für   |   Firefox für   |
|               |               |   Browser   |    Mobile   |   Android      |   Android       |
+===============+===============+=============+=============+================+=================+
| 3.Vorgänger-  | 4.0-4.1: Ja   | 3.0: Ja     | 11.5: Ja    | -              | -               |
| version       |               |             |             |                |                 |
+---------------+---------------+-------------+-------------+----------------+-----------------+
| 2.Vorgänger-  | 4.2-4.3: Ja   | 4.0: Ja     | 12.0: Ja    | -              | -               |
| version       |               |             |             |                |                 |
+---------------+---------------+-------------+-------------+----------------+-----------------+
| Vorherige     | 5.0-5.1: Ja   | 4.1: Ja     | 12.1: Ja    | -              | -               |
| Version       |               |             |             |                |                 |
+---------------+---------------+-------------+-------------+----------------+-----------------+
| Aktuelle      | 6.0-6.1: Ja   | 4.2: Ja     | 14.0: Ja    | 28.0: Ja       | 23.0: Ja        |
| Version       |               |             |             |                |                 |
+---------------+---------------+-------------+-------------+----------------+-----------------+
| Nächste       | 7.0: Ja       | -           | -           | -              | -               |
| Version       |               |             |             |                |                 |
+---------------+---------------+-------------+-------------+----------------+-----------------+

Aus diesen Vorbedingungen lässt sich für die VoD-Komponente die Konfiguration aus Tabelle 3.4 und für die Livestreaming Komponente die Konfiguration aus Tabelle 3.5 ableiten.

\newpage

: Mögliche Format/Auflösung/Bitraten-Verteilung Video-on-Demand

+---------------+----------------+---------+
| Auflösung[px] | Bitrate [kbps] | Codec   |
+===============+================+=========+
| 1920x1080     | 2500           | ogv     |
+---------------+----------------+---------+
| 720x480       | 1200           | ogv     |
+---------------+----------------+---------+
| 480x320       | 600            | ogv     |
+---------------+----------------+---------+
| 1920x1080     | 2500           | mpeg-4  |
+---------------+----------------+---------+
| 720x480       | 1200           | mpeg-4  |
+---------------+----------------+---------+
| 480x320       | 600            | mpeg-4  |
+---------------+----------------+---------+
| 1920x1080     | 2500           | webm    |
+---------------+----------------+---------+
| 720x480       | 1200           | webm    |
+---------------+----------------+---------+
| 480x320       | 600            | webm    |
+---------------+----------------+---------+

: Mögliche Format/Auflösung/Bitraten-Verteilung Livestreaming

+---------------+----------------+----------------+
| Auflösung[px] | Bitrate [kbps] |     Codec      |
+===============+================+================+
| 1920x1024     |  2000          | h264           |
+---------------+----------------+----------------+
| 1280x720      |  1200          | h264           |
+---------------+----------------+----------------+
| 850x480       |  650           | h264           |
+---------------+----------------+----------------+