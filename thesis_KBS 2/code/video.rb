# -*- encoding : utf-8 -*-
class Video < ActiveRecord::Base

  #associations for playlist
  has_many :videos_playlists
  has_many :playlists, :through => :videos_playlists

  attr_accessible :attachment, :name, :trainer, :length, :description, 
                  :views, :rating, :specification_attributes

  has_one :specification, as: :owner, :dependent => :destroy

  accepts_nested_attributes_for :specification

  has_attached_file :attachment, 
		:path => 
      "videos/:trainer/:style/:basename.:content_type_extension",
		:styles => { 
      :ogv_hd => { :geometry => "1920x1080>", :format => 'ogv'},
      :ogv_sd => { :geometry => "720x480>", :format => 'ogv'},
      :ogv_lo => { :geometry => "480x320>", :format => 'ogv'},
      :mp4_hd => { :geometry => "1920x1080>", :format => 'mp4'},
      :mp4_sd => { :geometry => "720x480>", :format => 'mp4'},
      :mp4_lo => { :geometry => "480x320>", :format => 'mp4'},
      :webm_hd => { :geometry => "1920x1080>", :format => 'webm'},
      :webm_sd => { :geometry => "720x480>", :format => 'webm'},
      :webm_lo => { :geometry => "480x320>", :format => 'webm'},
      :splash => { :geometry => "720x480>", :format => 'jpg', 
        :time => 10},
      :thumb => { :geometry => "200x200>", :format => 'jpg', 
        :time => 10 }
      },
    :processors => lambda { |a| a.video? ? [ :ffmpeg ] : [ :thumbnail ]}
  
  validates_attachment :attachment, :presence => true

  def video?
    [ 'application/x-mp4',
      'video/mpeg',
      'video/quicktime',
      'video/x-la-asf',
      'video/x-ms-asf',
      'video/x-msvideo',
      'video/x-sgi-movie',
      'video/x-flv',
      'flv-application/octet-stream',
      'video/3gpp',
      'video/3gpp2',
      'video/3gpp-tt',
      'video/BMPEG',
      'video/BT656',
      'video/CelB',
      'video/DV',
      'video/H261',
      'video/H263',
      'video/H263-1998',
      'video/H263-2000',
      'video/H264',
      'video/JPEG',
      'video/MJ2',
      'video/MP1S',
      'video/MP2P',
      'video/MP2T',
      'video/mp4',
      'video/MP4V-ES',
      'video/MPV',
      'video/mpeg4',
      'video/mpeg4-generic',
      'video/nv',
      'video/parityfec',
      'video/pointer',
      'video/webm',
      'video/raw',
      'video/rtx' ].include?(attachment.content_type)
  end

end
