jQuery(document).ready(function($) {

  function show_schedule(){
    LoadContent("/livestream/schedule", function(){
        $('.calendar').calendar(
          {
            lang: 'en'
          });
        $.getJSON('/livestream/schedule.json', {
          format: "json"
        })
        .done(function(data){
          $.each(data, function( i, json ){
            $('.calendar').calendarSetEvent({
              'date': json.starts_at.split('T')[0], 
              'text': "&Uuml;bungsleiter: " + 
              json.trainer + "<br>Uhrzeit: " + 
              json.starts_at.split('T')[1].slice(0,5) + 
              " Uhr" + 
              "<br>Beschreibung: " + 
              json.description});
          });
        });
        $('.calendar').tooltip({
          offset: [10, -10],
          content: function() {
              return $(this).attr('title');
          }
        });
    });
  }

  function index(){
    LoadContent('/livestream', function(){
      kickoff = $("#countdown_time").attr("data-seconds");
      setInterval(ctdwn(kickoff), 10000);
      $(".flowlive1").flowplayer({
          swf: "/flowplayer-5.4.2/flowplayer.swf",
          rtmp: 'rtmp://vm308.rz.uos.de/flvplayback',
          splash: true
      });
      $(".flowlive2").flowplayer({
          swf: "/flowplayer-5.4.2/flowplayer.swf",
          rtmp: 'rtmp://vm308.rz.uos.de/flvplayback',
          splash: true
      });
      $(".flowlive3").flowplayer({
          swf: "/flowplayer-5.4.2/flowplayer.swf",
          rtmp: 'rtmp://vm308.rz.uos.de/flvplayback',
          splash: true
      });
    });
  }
  $("#show_schedule").click(function(){
    show_schedule();
  });

  // Menu -> Livestreams
  $('#livestream').click(function(){
    index();

  });

});

function ctdwn(kickoff) {
    
    now = new Date();
    now.getTime();
    if (kickoff > 0)
    {
      diff = kickoff - now/1000;
      days  = Math.floor( diff / (60*60*24) );
      hours = Math.floor( diff / (60*60) );
      mins  = Math.floor( diff / (60) );
      secs  = Math.floor( diff );

      dd = days;
      hh = hours - days  * 24;
      mm = mins  - hours * 60;
      ss = secs  - mins  * 60;
      if(document.getElementById("countdown_time"))
        {
          document.getElementById("countdown_time").innerHTML =
          ' in ' +
          dd + ' Tage(n) ' +
          hh + ' Stunden und ' +
          mm + ' Minuten';
        }

    }
    else {
      if(document.getElementById("countdown_time"))
      {
        document.getElementById("countdown_time").innerHTML = ' noch nicht angek&uuml;ndigt';
      }
    }

}