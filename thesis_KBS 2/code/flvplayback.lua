--[[./config/flvplaback.lua]]--

configuration=
{
  daemon=false,
  pathSeparator="/",

  logAppenders=
  {
    {
      name="console appender",
      type="coloredConsole",
      level=6
    },
    {
      name="file appender",
      type="file",
      level=6,
      fileName="./logs/crtmpserver",
      fileHistorySize=10,
      fileLength=1024*1024,
      singleLine=true 
    }
  },
  
  applications=
  {
    rootDirectory="applications",
    {
      description="FLV Playback",
      name="flvplayback",
      protocol="dynamiclinklibrary",
      default=true,
      aliases=
      {
        "live"
      },
      acceptors = 
      {
        {
          ip="0.0.0.0",
          port=1935,
          protocol="inboundRtmp"
        },
      },
      validateHandshake=true,
      authentication=
      {
        rtmp={
          type="adobe",
          encoderAgents=
          {
            "FMLE/3.0 (compatible; FMSc/1.0)",
          },
          usersFile="users.lua"
        },
      },
    },
  }
}

